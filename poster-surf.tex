\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}
\usepackage{grffile}
\mode<presentation>{\usetheme{I6pd2}}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage[
backend=biber,
style=numeric-comp,
sorting=none,
natbib=true,
]{biblatex}
\addbibresource{posterbib.bib}
%\usepackage{times}\usefonttheme{professionalfonts}  % obsolete
%\usefonttheme[onlymath]{serif}
\boldmath
\usepackage[orientation=portrait,size=a0,scale=1.48,debug]{beamerposter}
% change list indention level
% \setdefaultleftmargin{3em}{}{}{}{}{}

\AtNextBibliography{\scriptsize}

%\usepackage{snapshot} % will write a .dep file with all dependencies, allows for easy bundling

\usepackage{array,booktabs,tabularx}
\newcolumntype{Z}{>{\centering\arraybackslash}X} % centered tabularx columns
\newcommand{\pphantom}{\textcolor{ta3aluminium}} % phantom introduces a vertical space in p formatted table columns??!!

\listfiles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\graphicspath{{figures/}}
\setlogo{figures/UniLogo}
\setauthorurl{https://www.hep.shef.ac.uk/research/muontomographyvolcanoes/}
\setauthoremail{harry.moss@sheffield.ac.uk}
\title{\huge Muon Tomography for the Cerro Mach\'{i}n Volcano}
\author{Harry Moss, Vitaly Kudryavtsev}
\institute[University of Sheffield]{Department of Physics \&
  Astronomy, University of Sheffield, Sheffield, United Kingdom}
\date[January 2019]{January, 2019}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newlength{\columnheight}
\setlength{\columnheight}{105cm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{frame}
  \begin{columns}
    % ---------------------------------------------------------%
    % Set up a column 
    \begin{column}{.49\textwidth}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{minipage}[T]{.95\textwidth}  % tweaks the width, makes a new \textwidth
          \parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
            % Since all columns are the same length, it is all nice and tidy.  You have to get the height empirically
            % ---------------------------------------------------------%
            % fill each column with content  
\vskip3ex          
            \begin{block}{Introduction: Muon Tomography for Cerro Mach\'{i}n}
              Muon tomography is presented as a technique for
              determining low density regions of the active volcano
              Cerro Mach\'{i}n situated near the city of Ibagu\'{e}, Colombia. The identification of such regions can
              inform volcanologists of the likely direction of ejecta
              following an eruption. The MuTe detector~\cite{MuTePaper}, constructed by
              colleagues at the University of Santander, is an
              adjustable detector composed of scintillating panels and
              a water Cherenkov detector designed for placement at the foot of the volcanic
              dome to measure variations in muon flux. A schematic of
              the detector is shown in figure \ref{fig:detectorSchematic}. This poster
              documents the work performed in generating simulations
              of the muons flux with CORSIKA~\cite{CORSIKAref} after their
              transport through the volcano using the MUSIC software
              framework~\cite{MUSICref} and reaching the suggested
              detector position shown in figure
              \ref{fig:2DcontourMap}.
  \begin{columns}
                \begin{column}{.45\textwidth}
\begin{figure}
\label{fig:2DcontourMap}
                  \includegraphics[width=\linewidth]{pythonDistance/CoordMapContour}
                  \caption{Contour map of the area surrounding Cerro
                    Mach\'{i}n highlighting the position of the
                    volcanic dome and the suggested detector
                    position. Elevations are shown in metres. The x and
                    y axes show latitude and longitude, respectively.}
\end{figure}         
\end{column}
\begin{column}{.45\textwidth}
\begin{figure}
\label{fig:detectorSchematic}
                  \includegraphics[width=\linewidth]{MuTeDetector}
                  \caption{Schematic view of the MuTe detector, with
                    water Cherenkov detector (left) and scintillating
                    tile (right) shown. Figure from \cite{MuTeSite}.}
\end{figure}  
\end{column}    
\end{columns}
            \end{block}
            \vfill
            \begin{block}{Topography of Volcano and Detector}
              \begin{columns}
                
                \begin{column}{.45\textwidth}
                  \begin{center}
\begin{figure}
\label{fig:CoordSystem}
                  \includegraphics[width=\linewidth]{VolcanoDiagram.pdf}
                  \caption{Coordinate system}
\end{figure}
                  \end{center}
\end{column}
\begin{column}{.5\textwidth}
\begin{figure}
\label{fig:MuonTrajectories}
                  \includegraphics[width=\linewidth]{pythonDistance/3D_rays}
                  \caption{Three-dimensional contour map showing
                    elevation in metres as a function of a locally
                    defined coordinate system. Possible trajectories for muons entering
                    the detector after passing through at least 10
                    metres of volcanic rock.}
\end{figure}
                \end{column}
              \end{columns}
            \end{block}
            \vfill
            \begin{block}{Generation of Muon Samples}
              \begin{itemize}
              \item{Muon samples are generated using
                  CORSIKA~\cite{CORSIKAref} with the QGSJET-II~\cite{QGSJETII} high energy
                  interaction model to shower primary cosmic
                  rays in the upper atmosphere with the following attributes:}
                \begin{itemize}
              \item{Primary particle zenith angle $60 \leq \theta \leq 90$, where
                    $\theta=0$ when particles are travelling
                    vertically downwards}
                \item{Primary particle energy $\leq 10^{6}$ GeV}
                  \end{itemize}
                \item{Particles identified as muons at
                    $2750~\textrm{m}$ above sea level are considered
                    for transport through the volcano.}
                  \item{The number of showers performed using CORSIKA
                      is dependent on the simulation time
                      considered. The results shown in this poster
                      represent the transport of muons through the
                      volcano over a period of three months.}
                \end{itemize}
            \end{block}
            \vfill
          }
        \end{minipage}
      \end{beamercolorbox}
    \end{column}
    % ---------------------------------------------------------%
    % end the column

    % ---------------------------------------------------------%
    % Set up a column 
    \begin{column}{.49\textwidth}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{minipage}[T]{.95\textwidth} % tweaks the width, makes a new \textwidth
          \parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
            % Since all columns are the same length, it is all nice and tidy.  You have to get the height empirically
            % ---------------------------------------------------------%
            % fill each column with content

\vskip3ex
            \begin{block}{Muon Transport}
              Muons are transported through the volcano using the
              MUSIC~\cite{MUSICref} software framework.
              \begin{itemize}
                \item{Muons identified in the output of the CORSIKA
                    simulation are stored at the maximum height of Cerro Mach\'{i}n}
                \item{A MATLAB simulation of the volcano topography
                    provides possible trajectories in
                    $x,y,\theta~\textrm{and}~\phi$ for detected muons
                    passing through $\geq 10~\textrm{m}$ of the
                    volcano (figure \ref{fig:MuonTrajectories}).}
                  \item{Output muons have $x,y,\phi$ 
                      coordinates randomly selected according to
                      possible trajectories derived from figure
                      \ref{fig:MuonTrajectories}, assuming an
                      isotropic collection of muons with no $\phi$-dependence.}
                      \item{An artificial low-density region of
                          $2.3~\textrm{g}~\textrm{cm}^{-3}$ is added
                          at the MUSIC transport stage. Outside of
                          this region, rock density is held at $2.5~\textrm{g}~\textrm{cm}^{-3}$.}
                      \item{MUSIC considers the profile of the
                          volcanic rock as determined by measurements
                          of samples of volcanic rock and accounts for
                        mineral composition, rock density and
                        thickness of rock.}
                \end{itemize}
            \end{block}
            \vfill
            \begin{block}{Results}
              \begin{columns}
                \begin{column}{.45\textwidth}
                 \begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{Results/RandomPhi/AngleVDistance_84Day.pdf}
 \caption[Distance-Angle relationship in the randomised-$\phi$ case]{Distance-Angle relationship in the randomised-$\phi$ case.}
\label{fig:MuonAngleVDistance_RandomPhi}          
    \end{figure}
                \end{column}
                \begin{column}{.45\textwidth}
                 \begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{Results/LowDensity/NumberOfMuons_perAngle_OneDeg_logColorBar_100Days.pdf}
 \caption[Number of muons expected after 100 days]{Number of muons
   expected in each
   $1\times{}1$ degree bin after 100 days of observation.}
\label{fig:NumberOfMuons100Days_lowDensity}          
    \end{figure}
                \end{column}
              \end{columns}

              \begin{columns}
                \begin{column}{.45\textwidth}
                 
\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{Results/AlejandraComp/AlejandraMUSICFlux_Ratio_goodCMap.pdf}
 \caption[Ratio of fluxes obtained from this and a previous study]{The
 ratio of muon fluxes from CORSIKA+MUSIC and a previous study by
 A. Vesga Ram\'{i}rez ($f_{\textrm{M}}/f_{\textrm{A}}$) encountered by a
detector situated at 4.492298 N, 75.381092 W.}
\label{fig:AlejandraFluxRatioComparison}          
    \end{figure}


                \end{column}
                \begin{column}{.45\textwidth}
                 
\begin{figure}[H]
       \centering
       \includegraphics[width=.9\textwidth]{Results/LowDensity/NumberOfMuons_perAngle_OneDeg_NOLOGColorBar_100Days_HighNumberLimit_1000.pdf}
 \caption[Number of muons expected after 100 days shown with a linear scale]{Number of muons
   expected in each
   $1\times{}1$ degree bin after 100 days of observation. This figure
   shows a linear scale and requires $\leq 1000$ muons in each bin.}
\label{fig:NumberOfMuons100Days_lowDensity_noLog1000}          
    \end{figure}

                \end{column}
              \end{columns}


            \end{block}
            \vfill
              \begin{block}{Summary}
              \begin{itemize}
              \item Studied muon fluxes and raw counts after transport
                with MUSIC through Cerro Mach\'{i}n
                \item{Simulation accurately replicates mathematical
                    model studied previously by Universidad Industrial de Santander}
                \item{Could not detect small changes in density in
                    volcano near volcanic dome}
                \end{itemize}
            \end{block}
            \vfill
          \begin{block}{References}
            \printbibliography
          \end{block}
          \vfill
}
          % ---------------------------------------------------------%
          % end the column
        \end{minipage}
      \end{beamercolorbox}
    \end{column}
    % ---------------------------------------------------------%
    % end the column
  \end{columns}
            
  \vskip1ex
  %\tiny\hfill\textcolor{ta2gray}{Created with \LaTeX \texttt{beamerposter}  \url{http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php}}
 % \tiny\hfill{Created with \LaTeX \texttt{beamerposter}  \url{http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php} \hskip1em}
\end{frame}
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% End:
